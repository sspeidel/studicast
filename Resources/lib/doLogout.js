function doLogout(Window) {
	
		/*
		* get local stored dUuid from db
		*/
		var db = Ti.Database.open('StudiCastDB');
		var dUuid = db.execute('SELECT content FROM StudiCastData WHERE name=?','dUuid').fieldByName('content');
		db.close();
		var data = {
			dUuid:dUuid
		};
		//send logout request to API
		var xhr = Ti.Network.createHTTPClient();
		xhr.open('POST',Ti.API.apiAdress+'/user/logout');
		xhr.send(data);		
		//Delete dUuid entry from StudiCastData and GoTo LoginWindow
		var db = Ti.Database.open('StudiCastDB');
		db.execute('DELETE FROM StudiCastData WHERE name=? OR name=?;','dUuid','uId');
		db.close();	
		//got to loginWindow
		var LoginWindow = require('ui/common/loginWindow');
		new LoginWindow(Window).open();
}

module.exports = doLogout;