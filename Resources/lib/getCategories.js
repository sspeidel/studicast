function getCategories(Window, callback, window, win1){

		/*
		 * get local stored dUuid from db
		 */
		var db = Ti.Database.open('StudiCastDB');
		var dUuid = db.execute('SELECT content FROM StudiCastData WHERE name= ?','dUuid').fieldByName('content');
		db.close();
		var data = {
			dUuid: dUuid
		};
		
		//handle json response
		var xhr = Ti.Network.createHTTPClient({
			onload: function(e,listData) {
					var listData =[];
				parsedData = JSON.parse(this.responseText);
				for(var cat in parsedData.categories){
					//build properties object for each categorie
					var categorie = { 
							properties: {
								 title: parsedData.categories[cat].catName,
								 id: parsedData.categories[cat].catId,
								 color: 'black',
								 font: { fontFamily:'Arial', fontSize: '17dp'}						 
							}
						};
					listData.push(categorie);
					}
					callback(Window, listData, window, win1);
						
				}
				
				
		
		});
		//open json request
		xhr.open('POST',Ti.API.apiAdress+'/category/getCategories',false);
		xhr.send(data);

}
module.exports = getCategories;