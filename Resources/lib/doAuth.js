function doAuth(dUuid,Window) {

		var data = {
			dUuid:dUuid
		};

		var xhr = Ti.Network.createHTTPClient({
			onload: function(e) {
				parsedData = JSON.parse(this.responseText);
				if(parsedData.isSuccess==false){ //if login is not current
					//Delete dUuid entry from StudiCastData and GoTo LoginWindow
					var db = Ti.Database.open('StudiCastDB');
					db.execute('DELETE FROM StudiCastData WHERE name=?;','dUuid');
					db.close();
					
					var db = Ti.Database.open('StudiCastDB');
					db.execute('DELETE FROM StudiCastData WHERE name=?;','uId');
					db.close();
					Titanium.UI.createAlertDialog({ title: L('loginFailed'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();
					var LoginWindow = require('ui/common/loginWindow');
					new LoginWindow(Window).open();
				}
				else{
					if(parsedData.isLoggedIn==true){ //on success go to MainWindow
						var ApplicationTabGroup = require('ui/common/ApplicationTabGroup');
						new ApplicationTabGroup(Window).open();
	   				}
	   				else{//if not logged in go to loginWindow
	   					var LoginWindow = require('ui/common/loginWindow');
						new LoginWindow(Window).open();
	   				}
				}
			},
			 // function called when an error occurs, including a timeout
     		onerror : function(e) {
     			if(dUuid!=null){ //if local dUuid is set, predict that user is logged in
	      			var ApplicationTabGroup = require('ui/common/ApplicationTabGroup');
					new ApplicationTabGroup(Window).open();    				
     			}
     			else{ //go to LoginWindow
     				var LoginWindow = require('ui/common/loginWindow');
					new LoginWindow(Window).open();
     			}
     		},
     		timeout : 5000  // in milliseconds
		});
		//open JSON request
		xhr.open('POST',Ti.API.apiAdress+'/user/authenticate');
		xhr.send(data);			
}

module.exports = doAuth;