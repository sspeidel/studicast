function downloadCast(Window,castData){
	var win = Ti.UI.createWindow({
			title:L('download'),
			backgroundColor:'white',
			layout:'vertical',
		});	
		var url = castData.cFileUrlMp3;
		
		var devHeight = (Ti.Platform.displayCaps.platformHeight/2);
		
	var loadingLabel = Ti.UI.createLabel({
	    text:L('downloading'),
	    top:devHeight,
	    color: 'black',
		font: { fontFamily:'Arial', fontSize: '17dp'}	
	});
	win.add(loadingLabel);
	 //download file to local filesystem from server
	var xhr = Titanium.Network.createHTTPClient({
	    onload: function() {

	        // first, grab a "handle" to the file where you'll store the downloaded data
	        var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,castData.fileName+'.mp3');
	        f.write(this.responseData); // write to the file

			//check if cat is available or needs update
			var db = Ti.Database.open('StudiCastDB');
			var checkCat = db.execute('SELECT * FROM StudiCastCat WHERE id= ?;',castData.catId);

			try{
				var checkCatName = checkCat.fieldByName('name');
			}
			catch(e){
				var checkCatName = null;
			}

			db.close();
			if(checkCatName==null){
				var db = Ti.Database.open('StudiCastDB');
				var insertCat = db.execute('INSERT INTO StudiCastCat (id,name) VALUES(?,?);',castData.catId,castData.categorie);
				db.close();
			}
			else if(checkCatName!=castData.catName){
				var db = Ti.Database.open('StudiCastDB');
				var updateCat = db.execute('UPDATE StudiCastCat SET name=? WHERE id=?;',castData.categorie,castData.catId);
				db.close();
			}
			//check if cast is already in DB
			var db = Ti.Database.open('StudiCastDB');	
			try{
				var checkCast = db.execute('SELECT * FROM StudiCastCasts WHERE id=?',castData.cId);
				var checkCastName = checkCast.fieldByName('cName');
			}
			catch(e){
				var checkCastName = null;
			}
			db.close();
			if(checkCastName==null){
				//insert cast to local db
				var db = Ti.Database.open('StudiCastDB');
				var insertCast = db.execute('INSERT INTO StudiCastCasts(id, catId, cName, cDescription, cTags, cFilename,cFileUrlMp3, cIsPublic, uName)	VALUES(?,?,?,?,?,?,?,?,?);', castData.id, castData.catId, castData.title, castData.description,	castData.tags,	castData.fileName,	castData.cFileUrlMp3, castData.isPublic, castData.name);
				db.close();
			}
		
	        Ti.App.fireEvent('downloaded', {filepath:f.nativePath});
	    },
	    timeout: 10000
	});
	xhr.open('GET',url);
	xhr.send();
	 
	Ti.App.addEventListener('downloaded', function(e) {
	    // you don't have to fire an event like this, but perhaps multiple components will
	    // want to know when the image has been downloaded and saved
	    win.remove(loadingLabel);
		win.close();
	});
	 
	win.open();	
}

module.exports = downloadCast;