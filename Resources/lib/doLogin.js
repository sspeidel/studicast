function doLogin(email,password,Window) {

		var data = {
			uEmail: email,
			uPassword: password,
			dName: Ti.Platform.id
		};

		//handle json response
		var xhr = Ti.Network.createHTTPClient({
			onload: function(e) {
				parsedData = JSON.parse(this.responseText);
				if(parsedData.isSuccess==false){ //error from API
					Titanium.UI.createAlertDialog({ title: L('loginFailed'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();
				}
				else{ //on success
					//Titanium.UI.createAlertDialog({ title: L('loginSuccess'), message: L('loginSuccessMessage')+' '+parsedData.uName+'!', buttonNames: ['OK'], cancel: 0 }).show();
					if(parsedData.isLoggedIn==true){ //iff login is successsfull set required data in local db
						var db = Ti.Database.open('StudiCastDB');
						db.execute('INSERT INTO StudiCastData(name,content) VALUES (?,?);','dUuid',parsedData.dUuid);
						db.close();
						
						var db = Ti.Database.open('StudiCastDB');
						db.execute('INSERT INTO StudiCastData(name,content) VALUES (?,?);','uId',parsedData.uId);
						db.close();
						//go to MainWindow
						var ApplicationTabGroup = require('ui/common/ApplicationTabGroup');
						new ApplicationTabGroup(Window).open();
	   				}
				}
			}
		});
		//open JSON request
		xhr.open('POST',Ti.API.apiAdress+'/user/login');
		xhr.send(data);			
}

module.exports = doLogin;