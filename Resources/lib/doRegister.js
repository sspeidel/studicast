function doRegister(email,name,password,Window) {

	if(email!='' && name!='' && password!=''){ //check that all data is filled
		var data = {
			uName: name,
			uEmail: email,
			uPassword: password,
			dName: Ti.Platform.id
		};
		
		//handle json response
		var xhr = Ti.Network.createHTTPClient({
			onload: function(e) {
				parsedData = JSON.parse(this.responseText);
				if(parsedData.isSuccess==false){ //failed error message from API
					Titanium.UI.createAlertDialog({ title: L('registrationFailed'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();
				}
				else{ //registration successfull
					Titanium.UI.createAlertDialog({ title: L('registrationSuccess'), message: L('registrationSuccessMessage'), buttonNames: ['OK'], cancel: 0 }).show();
					if(parsedData.isLoggedIn==true){ //if API returns loginSuccess add required localdata to db
						var db = Ti.Database.open('StudiCastDB');
						db.execute('INSERT INTO StudiCastData(name,content) VALUES (?,?);','dUuid',parsedData.dUuid);
						db.close();
						
						var db = Ti.Database.open('StudiCastDB');
						db.execute('INSERT INTO StudiCastData(name,content) VALUES (?,?);','uId',parsedData.uId);
						db.close();
						//go to main screen
						var ApplicationTabGroup = require('ui/common/ApplicationTabGroup');
						new ApplicationTabGroup(Window).open();
	   				}
	   			}
			}
		});
		//open json request
		xhr.open('POST',Ti.API.apiAdress+'/user/register');
		xhr.send(data);	
	}
	else return false;
}

module.exports = doRegister;