function searchCastsWindow(Window, win1){
	var self = Ti.UI.createWindow({
			title:L('searchCast'),
			backgroundColor:'white',
			layout:'vertical'
		});
		
	//SEARCH BAR
	var search = Titanium.UI.createSearchBar({
	   height:'43dp',
	   hintText:L('castName'),
	   top:'50dp', //dp-Values, else the Searchfield is cutOff
	   showCancel: false
	});
	 
	//AUTOCOMPLETE TABLE
	var table_data = [];
	var autocomplete_table = Titanium.UI.createTableView({
	   search: search,
	   scrollable: true,
	});
	
	self.add(autocomplete_table);
	
	//
	// SEARCH BAR EVENTS
	//
	var last_search='';
	search.addEventListener('change', function(e)
	{	
		//search only if the Text has more than 2 Chars and it isn't the last search
		if(search.value.length>2 && search.value != last_search){
			var data = {
				dUuid: dUuid,
				cSearch: search.value
				};
				
			//Get Casts to searchtext		
			var xhr = Ti.Network.createHTTPClient({
				onload: function(e,listData) {
					var table_data=[];
					parsedData = JSON.parse(this.responseText);
					if(parsedData.isSuccess){
						for(var cast in parsedData.casts){
							var pufferCast={properties: {
								 title: parsedData.casts[cast].cName,
								id: parsedData.casts[cast].cId,
								tags: parsedData.casts[cast].cTags,
								 description:parsedData.casts[cast].cDescription,
								 fileSize:parsedData.casts[cast].cFileSize,
								 fileType:parsedData.casts[cast].cFileType,
								 fileName:parsedData.casts[cast].cFileName,
								 isPublic:parsedData.casts[cast].cIsPublic,
								 cFileUrlMp3: parsedData.casts[cast].cFileUrlMp3,
								 downloads:parsedData.casts[cast].cDownloads,
								 name:parsedData.casts[cast].uName,
								 authorId:parsedData.casts[cast].uId,
								 bookmarked:parsedData.casts[cast].cIsBookmarked,
								 categorie:parsedData.casts[cast].catName,
								 catId:parsedData.casts[cast].catId,
								 color: 'black',
							 	 font: { fontFamily:'Arial', fontSize: '17dp'}	,
							 	 cRating:parsedData.casts[cast].cRating ,
							 	 uRating:parsedData.casts[cast].uRating,
							 	 uComment:parsedData.casts[cast].uComment	
							}
						};
						
						//foreach Cast a TableViewRow					
					  var row=Ti.UI.createTableViewRow(
		               {
		                  height: 40,
		                  title: pufferCast.properties.title,
		                  properties: pufferCast,      //to use the Cast             
		                  font: { fontFamily:'Arial', fontSize: '17dp'},
		                  color: 'black'
	           
		               });
		               
		            //Eventlistener for TableViewRow, open the Castplayer   
		            row.addEventListener('click', function(e){
	  					var goToPlayer = require('ui/common/castPlayerWindow');
						win1.containingTab.open(new goToPlayer(Window,win1,e.source.properties)); 
					});
					 
					table_data.push(row);
					}				
					//to show the casts in a Collection
					autocomplete_table.setData(table_data);	
				}
			}
		});
		//open json request
		xhr.open('POST',Ti.API.apiAdress+'/cast/getCastsByString',false);
		xhr.send(data);
		last_search=search.value;
	  }
});

//get dUuid from Database for json request
 var db = Ti.Database.open('StudiCastDB');
	var dUuid = db.execute('SELECT content FROM StudiCastData WHERE name= ?','dUuid').fieldByName('content');
	db.close();
	
	
	
	return self;
};

module.exports = searchCastsWindow;