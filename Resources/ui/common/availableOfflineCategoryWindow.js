function availableOfflineCategoryWindow(Window,win1){
var self = Ti.UI.createWindow({
		title:L('availableOfflineCasts'),
		backgroundColor:'white',
		layout:'vertical'
	});

		var listData =[];
		var db = Ti.Database.open('StudiCastDB');
		var rows = db.execute('SELECT * FROM StudiCastCat');
		//read all categories
		while (rows.isValidRow()){
			//build properties object for each categorie
			var categorie = { 
				properties: {
					title: rows.fieldByName('name'),
					id: rows.fieldByName('id'),
					color: 'black',
					font: { fontFamily:'Arial', fontSize: '17dp'}						 
				}
			};
			listData.push(categorie);
			rows.next();
		}
		rows.close();
		db.close();
		//if no data is recieved go back
		if(listData[0]==null){
			Titanium.UI.createAlertDialog({ title: L('error'), message: L('errorNoOfflineCasts'), buttonNames: ['OK'], cancel: 0 }).show();
			self.close();
		}		

				// Add the list data items to a section
				var listSection = Titanium.UI.createListSection({items: listData});
				// Add the list section to a list view
				var listView = Titanium.UI.createListView({sections: [listSection]});
				//on click listener		
				listView.addEventListener('itemclick', function(e){
				    var item = e.section.getItemAt(e.itemIndex);
				    //go to the list of casts in the respective categorie
				    e.section.updateItemAt(e.itemIndex, item);  
				    	var goToAvailableOfflineCastsToCategory = require('ui/common/availableOfflineCastsToCategoryWindow');
						win1.containingTab.open(new goToAvailableOfflineCastsToCategory(Window,win1,item));   
				});	
				self.add(listView);

		return self;
};

module.exports = availableOfflineCategoryWindow;