function LoginWindow(Window) {
	var self = Ti.UI.createWindow({
		title:Window,
		backgroundColor:'white',
		layout:'vertical'
	});


	/*
	 * create UI with title and textfields
	 */	
	var appName = Ti.UI.createLabel({
		text:L('appName'),
		top:20,
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
		width:Ti.UI.SIZE,
		font: { fontFamily:'Arial', fontSize: '25dp'}	
	});
	self.add(appName);
	
	var tb_username = Ti.UI.createTextField({
		hintText:L('username'),
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		width:Ti.UI.FILL,
		top:10,
		autocorrect:false,
		
	});
	self.add(tb_username);
	
	var tb_password = Ti.UI.createTextField({
		hintText:L('password'),
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		top:5,
		passwordMask:true,
		width:Ti.UI.FILL
	});
	self.add(tb_password);
	
	var bu_login = Ti.UI.createButton({
		title:L('login'),
		top:10,
		width:Ti.UI.FILL
	});
	
	
	//onclick listener function for login click
	bu_login.addEventListener('click',function(e)
	{
	   doLogin = require('lib/doLogin');
	   if(Ti.Network.online==1){ //if active network connection do login
	   	execLogin = doLogin(tb_username.value,tb_password.value,Window);
	   }
	   else{ //error if no network connection
	   	Titanium.UI.createAlertDialog({ title: L('offline'), message: L('loginOffline'), buttonNames: ['OK'], cancel: 0 }).show();
	   }
	   
	});	
	
	self.add(bu_login);
	
	//registerbutton-link with onclick listener
	var bu_register = Ti.UI.createButton({
		title:L('needRegister'),
		top:10,
		width:Ti.UI.FILL
	});
	bu_register.addEventListener('click',function(e)
	{
	   	var goToRegister = require('ui/common/registerWindow');
		new goToRegister(Window).open();
		
	});	
	self.add(bu_register);
	
	return self;
};

module.exports = LoginWindow;