function ApplicationTabGroup(Window) {
	//create tabgroup module instance
	var self = Ti.UI.createTabGroup();
	
	//create app tabs
	var win1 = new Window(L('menue')),
		win2 = new Window(L('settings'));
	
	//set layout	
	win1.layout='vertical';
	win2.layout='vertical';
	win1.width=Ti.UI.Fill;
	
	//create menu tab
	var tab1 = Ti.UI.createTab({
		title: L('home'),
		icon: '/images/KS_nav_ui.png',
		layout:'vertical',
		window: win1
	});
	win1.containingTab = tab1;
	
	/*
	 * add menu buttons with respective onclick listener
	 */
	/*
	var bu_playlist = Ti.UI.createButton({
		title:L('playlists'),
		top:10,
		width:Ti.UI.FILL
	});
	bu_playlist.addEventListener('click',function(e)
	{
	   	var goToPlaylist = require('ui/common/playlistWindow');
		//new goToPlaylist(Window).open();
		win1.containingTab.open(new goToPlaylist(Window,win1));
		
	});	
	win1.add(bu_playlist);*/
	if(Ti.Network.online==1){ 
		var bu_searchCasts = Ti.UI.createButton({
			title:L('searchCasts'),
			top:10,
			width:Ti.UI.FILL
		});
		bu_searchCasts.addEventListener('click',function(e)
		{
		   	var goToSearchCasts = require('ui/common/searchCastsWindow');
			//new goToPlaylist(Window).open();
			win1.containingTab.open(new goToSearchCasts(Window,win1));
			
		});	
		win1.add(bu_searchCasts);
		
		var bu_myCasts = Ti.UI.createButton({
			title:L('myCasts'),
			top:10,
			width:Ti.UI.FILL
		});
		bu_myCasts.addEventListener('click',function(e)
		{
		   	var goToMyCasts = require('ui/common/myCastsWindow');
			win1.containingTab.open(new goToMyCasts(Window,win1));
			
		});	
		win1.add(bu_myCasts);
		
		var bu_allCasts = Ti.UI.createButton({
			title:L('allCasts'),
			top:10,
			width:Ti.UI.FILL
		});
		bu_allCasts.addEventListener('click',function(e)
		{
		   	var goToAllCasts = require('ui/common/allCastsWindow');
			win1.containingTab.open(new goToAllCasts(Window,win1));
			
		});	
		win1.add(bu_allCasts);	

		var bu_Bookmarks = Ti.UI.createButton({
			title:L('bookmarked'),
			top:10,
			width:Ti.UI.FILL
		});
		bu_Bookmarks.addEventListener('click',function(e)
		{
		   	var goToBookmarks = require('ui/common/bookmarksWindow');
			win1.containingTab.open(new goToBookmarks(Window,win1));
			
		});	
		win1.add(bu_Bookmarks);	
		
		var bu_beforeRecord = Ti.UI.createButton({
			title:L('record'),
			top:10,
			width:Ti.UI.FILL
		});
		if(Ti.Platform.osname!='android'){ //switch for android, because android can't record under TI
			bu_beforeRecord.addEventListener('click',function(e)
			{
			   	var goToBeforeRecord = require('ui/common/beforeRecordWindow');
				win1.containingTab.open(new goToBeforeRecord(Window,win1));
				
			});	
			win1.add(bu_beforeRecord);	
		}
	}
	else{
		var bu_availOffline = Ti.UI.createButton({
			title:L('availableOfflineCasts'),
			top:10,
			width:Ti.UI.FILL
		});
		bu_availOffline.addEventListener('click',function(e)
		{
		   	var availableOfflineCategoryWindow = require('ui/common/availableOfflineCategoryWindow');
			win1.containingTab.open(new availableOfflineCategoryWindow(Window,win1));
			
		});	
		win1.add(bu_availOffline);
	}
	
	
	var tab2 = Ti.UI.createTab({
		title: L('settings'),
		icon: '/images/KS_nav_views.png',
		layout: 'vertical',
		height:'30dp',
		width:'30dp',
		window: win2
	});
	win2.containingTab = tab2;
	if(Ti.Network.online==1){ 	
		var bu_Logout = Ti.UI.createButton({
			title: L('logout'),
			top:10,
			width:Ti.UI.FILL
		});
		bu_Logout.addEventListener('click',function(e)
		{
			doLogout = require('lib/doLogout');
		   	execLogout = doLogout(Window);		
		});	
		win2.add(bu_Logout);	
	}	

	var bu_emptyDB = Ti.UI.createButton({
		title: 'App zurücksetzen',
		top:10,
		width:Ti.UI.FILL
	});
	bu_emptyDB.addEventListener('click',function(e)
	{
		//drop all tables
		var db = Ti.Database.open('StudiCastDB');	
		db.execute('DROP TABLE StudiCastData;');
		db.close();
	
		var db = Ti.Database.open('StudiCastDB');	
		db.execute('DROP TABLE StudiCastCat;');
		db.close();
		
		var db = Ti.Database.open('StudiCastDB');		
		db.execute('DROP TABLE StudiCastCasts;');
		db.close();	

		var db = Ti.Database.open('StudiCastDB');		
		db.execute('CREATE TABLE IF NOT EXISTS StudiCastData(name TEXT PRIMARY KEY, content TEXT);');
		db.close();
		
		var db = Ti.Database.open('StudiCastDB');		
		db.execute('CREATE TABLE IF NOT EXISTS StudiCastCat(id INTEGER PRIMARY KEY, name TEXT);');
		db.close();
		
		var db = Ti.Database.open('StudiCastDB');		
		db.execute('CREATE TABLE IF NOT EXISTS StudiCastCasts(id INTEGER PRIMARY KEY, catId INTEGER, cName TEXT, cDescription TEXT, cTags TEXT, cFilename TEXT,cFileUrlMp3 TEXT, cIsPublic BOOL, uName TEXT);');
		db.close();
		
		var resourcesDir = Titanium.Filesystem.getApplicationDataDirectory();
        var dir = Titanium.Filesystem.getFile(resourcesDir);
        var dir_files = dir.getDirectoryListing();
        for (var i=1;i<dir_files.length;i++){ 
        	var thisFile = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,dir_files[i]);
        	if(thisFile.exists() && dir_files[i]!='Dummy.mp3'){
        		thisFile.deleteFile();
        	}	
        }
		
		//got to loginWindow
		var LoginWindow = require('ui/common/loginWindow');
		new LoginWindow(Window).open();		
	});	
	win2.add(bu_emptyDB);
	
	self.addTab(tab1);
	self.addTab(tab2);
	
	return self;
};

module.exports = ApplicationTabGroup;