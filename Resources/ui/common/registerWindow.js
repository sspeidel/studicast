function RegisterWindow(Window) {
	var self = Ti.UI.createWindow({
		title:Window,
		backgroundColor:'white',
		layout:'vertical'
	});
	
	/*
	 * create UI with title and textfields
	 */
	var appName = Ti.UI.createLabel({
		text:L('appName')+' - '+L('register'),
		font: {fontSize: 30},
		top:20,
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
		width:Ti.UI.SIZE
	});
	self.add(appName);


	var tb_email = Ti.UI.createTextField({
		hintText:L('username'),
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		width:Ti.UI.FILL,
		top:10,
		autocorrect:false,
		
	});
	self.add(tb_email);

	var tb_name = Ti.UI.createTextField({
		hintText:L('fullName'),
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		width:Ti.UI.FILL,
		top:5,
		autocorrect:true,
		
	});
	self.add(tb_name);
	
	var tb_password = Ti.UI.createTextField({
		hintText:L('password'),
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		top:5,
		passwordMask:true,
		width:Ti.UI.FILL,
	});
	self.add(tb_password);

	
	var bu_register = Ti.UI.createButton({
		title:L('doRegister'),
		width:Ti.UI.FILL,
		top:10,
	});
	
	/*
	 * add eventlistener for click on register
	 */
	bu_register.addEventListener('click',function(e)
	{
	   doRegister = require('lib/doRegister');
	   execRegister = doRegister(tb_email.value,tb_name.value,tb_password.value,Window); //execute registration
	});	
	
	self.add(bu_register);
	
	/*
	 * Backbutton eventlistener
	 */
	var bu_back = Ti.UI.createButton({
		title:L('backToLogin'),
		width:Ti.UI.FILL,
		top:5,
	});
	bu_back.addEventListener('click',function(e)
	{
		self.close();
	});
	self.add(bu_back);
	
	return self;
};

module.exports = RegisterWindow;