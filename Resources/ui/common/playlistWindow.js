function playlistWindow(Window, win1){
var self = Ti.UI.createWindow({
		title:L('playlists'),
		backgroundColor:'white',
		layout:'vertical'
	});

// Array der anzuzeigenden Elemente, ggf direkt Casts verwendbar
var data = [
    { properties: { title: 'Row 1'} },
    { properties: { title: 'Row 2'} },
    { properties: { title: 'Row 3'} }
];

// Add the list data items to a section
var listSection = Titanium.UI.createListSection({items: data});
 
// Add the list section to a list view
var listView = Titanium.UI.createListView({sections: [listSection]});
	
		
listView.addEventListener('itemclick', function(e){
    var item = e.section.getItemAt(e.itemIndex);
    
    // folgendes wird zum anhaken von Elementen benötigt
   // if (item.properties.accessoryType == Ti.UI.LIST_ACCESSORY_TYPE_NONE) {
    //    item.properties.accessoryType = Ti.UI.LIST_ACCESSORY_TYPE_CHECKMARK;
   // }
   // else {
    //    item.properties.accessoryType = Ti.UI.LIST_ACCESSORY_TYPE_NONE;
   // }
    
    //hier Aktion durchführen, die mit item durchgeführt werden soll
    e.section.updateItemAt(e.itemIndex, item);  
    alert(
        item.properties.title
    ); 
    var goToMyCasts = require('ui/common/myCastsWindow');
	win1.containingTab.open(goToMyCasts(Window));   
});		
		
	self.add(listView);	
		
		
		
	return self;
};

module.exports = playlistWindow;