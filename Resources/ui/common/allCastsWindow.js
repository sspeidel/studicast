function allCastsWindow(Window,win1){
var self = Ti.UI.createWindow({
		title:L('allCasts'),
		backgroundColor:'white',
		layout:'vertical'
	});

		//read all categories, callback is show function
		getCategories=require('lib/getCategories');
		var listData =  getCategories(Window, showCategories, self, win1);
		return self;
	}
	
	function showCategories(Window, listData, self, win1){
				// Add the list data items to a section
				var listSection = Titanium.UI.createListSection({items: listData});
				// Add the list section to a list view
				var listView = Titanium.UI.createListView({sections: [listSection]});
				//on click listener		
				listView.addEventListener('itemclick', function(e){
				    var item = e.section.getItemAt(e.itemIndex);
				    //go to the list of casts in the respective categorie
				    e.section.updateItemAt(e.itemIndex, item);  
				    	var goToAllCasts = require('ui/common/allCastsToCategorieWindow');
						win1.containingTab.open(new goToAllCasts(Window,win1,item));   
				});	
				self.add(listView);
};

module.exports = allCastsWindow;