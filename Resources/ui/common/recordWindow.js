function recordWindow(Window,win1,categorie){
var self = Ti.UI.createWindow({
		title:L('record'),
		backgroundColor:'white',
		layout:'vertical'
	});

	//Name des Casts
	var tb_name = Ti.UI.createTextField({
		hintText:L('castName'),
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		width:Ti.UI.FILL,
		top:10,
		autocorrect:true,
		
	});
	self.add(tb_name);
	
	//cTags
	var tb_tags = Ti.UI.createTextField({
		hintText:L('tags'),
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		width:Ti.UI.FILL,
		top:10,
		autocorrect:true,
		
	});
	self.add(tb_tags);	
	
	//cDescription
	var tb_description = Ti.UI.createTextField({
		hintText:L('description'),
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		width:Ti.UI.FILL,
		top:10,
		autocorrect:true,
		
	});
	self.add(tb_description);
 	
 	//Switch muss als eigene View definiert werden, damit Beschriftung korrekt angezeigt wird
	//cIsPublic 
	var isPublic= Ti.UI.createView({
		width:Ti.UI.FILL,
		layout:'horizontal',
		height:30
	});
	
	//Label des Switch
	var lb_isPublic=Ti.UI.createLabel({
		//width:Ti.UI.FILL,
		width:250,
		text:L('publicCast')
	});
	
	//Switch selbst
	var sw_isPublic = Ti.UI.createSwitch({
		value:false
	});
	isPublic.add(lb_isPublic);
	isPublic.add(sw_isPublic);
	self.add(isPublic);

	//AudioMode setzen
	Titanium.Media.audioSessionMode = Ti.Media.AUDIO_SESSION_MODE_RECORD;
	
	//recorder definieren
	var recorder=Ti.Media.createAudioRecorder({
		format:Ti.Media.AUDIO_FILEFORMAT_AAC,
		bubbleParent:false
	});
	
	//Start Button
	var bu_start=Ti.UI.createButton({
	title: L('home')	
	});
	
	bu_start.addEventListener("click", function(e){
		//Aufnahme starten
 		recorder.start();
 		Ti.API.info(recorder.getRecording());
 		
 	});
	
	self.add(bu_start);
	
	//Stop Button
	var bu_stopp=Ti.UI.createButton({
		title:L('stop')
	});
	
	var file;
	
	bu_stopp.addEventListener("click", function(e){
		//Recorder stoppen und Datei speichern
		if(recorder.getRecording){
			Ti.API.info(recorder.getRecording());
			
			//Auskommentiert wegen Dummydatei
 			//file= recorder.stop();
 			
			//Dummyfile muss in Documentverzeichnis der App liegen, und vor jedem Upload dort wiederreinkopiert werden.
 			file=Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory+'Dummy.mp3'); 
 		}
 	});
 	self.add(bu_stopp);
	
	//Button für Upload
	var bu_upload=Ti.UI.createButton({
		title: L('upload')
	});
	
	bu_upload.addEventListener("click", function(e){
		
 		//dUuid aus DB holen
 		var db = Ti.Database.open('StudiCastDB');
		var dUuid = db.execute('SELECT content FROM StudiCastData WHERE name= ?','dUuid').fieldByName('content');
		db.close();
		
 		//Daten für Upload zusammenstellen
 		var data={
 			dUuid: dUuid,
 			cFile: file.blob,
 			cName: tb_name.value,
 			cTags: tb_tags.value,
 			cDescription:tb_description.value,
 			cIsPublic:sw_isPublic.value,
 			catId:categorie.properties.id
 		};
 		//alert(data);
 	
 	//	Verbindung zur Schnittstelle
 	var xhr = Ti.Network.createHTTPClient({
			onload: function(e) {
				parsedData = JSON.parse(this.responseText);
				alert(parsedData);
				if(parsedData.isSuccess){
					/*Soll-Ergebnis vom Server:
 					 * {"isLoggedIn":true,"isSuccess":true,"uId":"19","cId":10,"cFileName":"52822fb11a737","cFileType":"audio\/mpeg"}
 					 */
 					alert(L('success'));
 					file.copy(Titanium.Filesystem.applicationDataDirectory + parsedData.cFileName + ".mp3");
				}
				else{
					Titanium.UI.createAlertDialog({ title: L('error'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();
					self.close();
				}

			}
		});
		xhr.open('POST',Ti.API.apiAdress+'/cast/upload');
		xhr.send(data);

 	});
 	self.add(bu_upload);
	return self;
}

module.exports = recordWindow;