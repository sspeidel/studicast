function availableOfflineCastsToCategory(Window,win1,categorie){
var self = Ti.UI.createWindow({
		title:categorie.properties.title,
		backgroundColor:'white',
		layout:'vertical'
	});

		/*
		 * get local stored dUuid from db
		 */
		var db = Ti.Database.open('StudiCastDB');
		var dUuid = db.execute('SELECT content FROM StudiCastData WHERE name= ?','dUuid').fieldByName('content');
		db.close();
		//build data object
		var data = {
			catId:categorie.properties.id,
			dUuid: dUuid
		};
		var listData =[];
		var db = Ti.Database.open('StudiCastDB');
		var rows = db.execute('SELECT * FROM StudiCastCasts WHERE catId=?',categorie.properties.id);
		//read all categories
		while (rows.isValidRow()){
			//build properties object for each categorie
						var cast = { 
								properties: {
									 title: rows.fieldByName('cName'),
									 id: rows.fieldByName('id'),
									 tags: rows.fieldByName('cTags'),
									 description:rows.fieldByName('cDescription'),
									 fileSize:0,
									 fileType:0,
									 fileName:rows.fieldByName('cFileName'),
									 isPublic:rows.fieldByName('cIsPublic'),
									 downloads:'n.a.',
									 name:rows.fieldByName('uName'),
									 authorId:0,
									 bookmarked:0,
									 categorie:categorie.properties.title,
									 catId:categorie.properties.id,
									 color: 'black',
								 	 font: { fontFamily:'Arial', fontSize: '17dp'}	
								}
							};
			listData.push(cast);
			rows.next();
		}
		rows.close();
		db.close();

		if(listData[0]==null){
			Titanium.UI.createAlertDialog({ title: L('errorNoOfflineCasts'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();
			self.close();
		}
				// Add the list data items to a section
				var listSection = Titanium.UI.createListSection({items: listData});	 
				// Add the list section to a list view
				var listView = Titanium.UI.createListView({sections: [listSection]});
				listView.addEventListener('itemclick', function(e){
				var item = e.section.getItemAt(e.itemIndex); 
				//go to cast Player of selected cast
				e.section.updateItemAt(e.itemIndex, item);  
				var goToPlayer = require('ui/common/castPlayerWindow');
				win1.containingTab.open(new goToPlayer(Window,win1,item));     
				});		
						
				self.add(listView);	
			

	
	return self;
};

module.exports = availableOfflineCastsToCategory;