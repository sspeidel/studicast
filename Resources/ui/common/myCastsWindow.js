function myCastsWindow(Window,win1){
var self = Ti.UI.createWindow({
		title:L('myCasts'),
		backgroundColor:'white',
		layout:'vertical'
	});
	
		/*
		 * get local stored uId and dUuid from db
		 */
		var db = Ti.Database.open('StudiCastDB');
		var dUuid = db.execute('SELECT content FROM StudiCastData WHERE name= ?','dUuid').fieldByName('content');
		db.close();
		
		var db = Ti.Database.open('StudiCastDB');
		var uId = db.execute('SELECT content FROM StudiCastData WHERE name= ?','uId').fieldByName('content');
		db.close();
		
		uId = parseInt(uId);
		
		var data = {
			uId:uId,
			dUuid: dUuid
		};
		
		var listData =[];
		//json request handling
		var xhr = Ti.Network.createHTTPClient({
			onload: function(e) {
				parsedData = JSON.parse(this.responseText);
				if(parsedData.isSuccess){
					for(var cat in parsedData.casts){
						//on success build object for each dataset
						var cast = { 
								properties: {
									 title: parsedData.casts[cat].cName,
									 id: parsedData.casts[cat].cId,
									 tags: parsedData.casts[cat].cTags,
									 description:parsedData.casts[cat].cDescription,
									 fileSize:parsedData.casts[cat].cFileSize,
									 fileType:parsedData.casts[cat].cFileType,
									 fileName:parsedData.casts[cat].cFileName,
									 isPublic:parsedData.casts[cat].cIsPublic,
									 cFileUrlMp3:parsedData.casts[cat].cFileUrlMp3,
									 downloads:parsedData.casts[cat].cDownloads,
									 name:parsedData.casts[cat].uName,
									 authorId:parsedData.casts[cat].uId,
									 bookmarked:parsedData.casts[cat].cIsBookmarked,
									 categorie:parsedData.casts[cat].catName,
									 catId:parsedData.casts[cat].catId,
									 color: 'black',
								 	 font: { fontFamily:'Arial', fontSize: '17dp'}	,
								 	 cRating:parsedData.casts[cat].cRating ,
								 	 uRating:parsedData.casts[cat].uRating,
								 	 uComment:parsedData.casts[cat].uComment						 
									 						 
								}
							};
						listData.push(cast);
					}
					// Add the list data items to a section
					var listSection = Titanium.UI.createListSection({items: listData});
					 
					// Add the list section to a list view
					var listView = Titanium.UI.createListView({sections: [listSection]});
						
					//onclick listener for each item		
					listView.addEventListener('itemclick', function(e){
					    var item = e.section.getItemAt(e.itemIndex);
					     
					    //switch to castPlayer on click
					    e.section.updateItemAt(e.itemIndex, item);  
					    var goToPlayer = require('ui/common/castPlayerWindow');
							win1.containingTab.open(new goToPlayer(Window,win1,item)); 
					    
					});		
					self.add(listView);	
				}
				else{ //on error
					Titanium.UI.createAlertDialog({ title: L('error'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();
					self.close();
				}				
			}
		});
		//open json request
		xhr.open('POST',Ti.API.apiAdress+'/cast/getCastsByUserId');
		xhr.send(data);
		
	return self;
};

module.exports = myCastsWindow;