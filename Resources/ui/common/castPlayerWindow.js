function castPlayerWindow(Window,self, cast){
var self = Ti.UI.createWindow({
		title:cast.properties.title,
		backgroundColor:'white',
		layout:'vertical',
	});
	
		var db = Ti.Database.open('StudiCastDB');
		var uId = db.execute('SELECT content FROM StudiCastData WHERE name= ?','uId').fieldByName('content');
		db.close();
						/*
						 * check if file is on local file system
						 */
						var isOffline;
						var filecheck = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, cast.properties.fileName+'.mp3');
						if(filecheck.exists()) { 
							var file = Titanium.Filesystem.applicationDataDirectory+cast.properties.fileName+'.mp3';
							isOffline=true;
						}
						else{
							var file = cast.properties.cFileUrlMp3;	
							isOffline=false;
						}
						/*
						 * create video player because it's eierlegendewollmilchsau
						 */
						var videoPlayer = Titanium.Media.createVideoPlayer({
						    top : 10,
						    autoplay : false,
						    backgroundColor : 'transparent',
						    height : 100,
						    width:Ti.UI.FILL,
						    mediaControlStyle : Titanium.Media.VIDEO_CONTROL_EMBEDDED,
						    scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FIT
						});
						videoPlayer.url = file;
						self.add(videoPlayer);
						
					//Show Author of the Cast
						var row1 = Ti.UI.createTableViewRow({
						    height:'auto',
						    selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
						});
						var infoAuthor = Ti.UI.createLabel({
							text:L('author')+': '+cast.properties.name,
							top:5,
							left:0,
							color: 'black',
							font: { fontFamily:'Arial', fontSize: '17dp'}	
						});
						
						row1.add(infoAuthor);
						
						//Show Categorie of the Cast
						var row2 = Ti.UI.createTableViewRow({
						    height:'auto',
						    selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
						});
						var infoCategory = Ti.UI.createLabel({
							text:L('category') + ': ' + cast.properties.categorie,
							top:5,
							width:Ti.UI.FILL,
							color: 'black',
							font: { fontFamily:'Arial', fontSize: '17dp'}
						});
						row2.add(infoCategory);
						
						
						
						//If the System is online, it show the countedDownloads
						var row3 = Ti.UI.createTableViewRow({
						    height:'auto',
						    selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
						});
						var infoDownloads = Ti.UI.createLabel({
							text:L('downloads')+': '+cast.properties.downloads,
							top:5,
							width:Ti.UI.FILL,
							color: 'black',
							font: { fontFamily:'Arial', fontSize: '17dp'}
						});
						
						row3.add(infoDownloads);
						
						//Show the Description of the Cast
						var row4 = Ti.UI.createTableViewRow({
						    height:'auto',
						    selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
						});
						var infoDescription = Ti.UI.createLabel({
							text:L('description')+': '+cast.properties.description,
							top:5,
							left:0,
							color: 'black',
							font: { fontFamily:'Arial', fontSize: '17dp'}
						});	

						row4.add(infoDescription);

						
						//Switch isOfflineAvaible
						var row5 = Ti.UI.createTableViewRow({
						    height:'auto',
						    selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
						});
						var lb_isOffline=Ti.UI.createLabel({
								width:250,
								left:0,
								text:L('availableOfflineCasts'),
								color: 'black',
								font: { fontFamily:'Arial', fontSize: '17dp'}
							});
						var sw_isOffline = Ti.UI.createSwitch({
							value:isOffline,
							right:10
						});
						
						sw_isOffline.addEventListener('change',function(e){
							if(sw_isOffline.value==1){
								//download file to local filesystem and hide download button
								downloadCast = require('lib/downloadCast');
								var doDownload = downloadCast(Window,cast.properties); 
								var file = Titanium.Filesystem.applicationDataDirectory+cast.properties.fileName+'.mp3';
							}
							else{
								//delete file from filesystem and remove DB entry for this cast
								var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,cast.properties.fileName+'.mp3');
								if(file.exists())	file.deleteFile();
								var db = Ti.Database.open('StudiCastDB');
								var delteCast= db.execute('DELETE FROM StudiCastCasts WHERE id=?;',cast.properties.id);
								db.close();
							}
						});
						row5.add(lb_isOffline);
						row5.add(sw_isOffline);
						
						//Switch isBookmarked
						var row6 = Ti.UI.createTableViewRow({
						    height:'auto',
						    selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
						});
						var lb_isBookmarked=Ti.UI.createLabel({
								width:250,
								left:0,
								text:L('bookmarked'),
								color: 'black',
								font: { fontFamily:'Arial', fontSize: '17dp'}
							});
							
						var sw_isBookmarked = Ti.UI.createSwitch({
							value:cast.properties.bookmarked,
							right:10
						});

						sw_isBookmarked.addEventListener('change',function(e){
							//Which API do we need
							var addOrRemove= (sw_isBookmarked.value) ? '/bookmark/addBookmarkToCast' : '/bookmark/removeBookmarkToCast';
							var data={
										dUuid:dUuid,
										cId:cast.properties.id
								};
								var xhr = Ti.Network.createHTTPClient({
										onload: function(e,listData) {
										parsedData = JSON.parse(this.responseText);
											//If something is going wrong, tell it
											if(!parsedData.isSuccess){
												Titanium.UI.createAlertDialog({ title: L('error'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();
											}
										}
								    });
								//open json request
								xhr.open('POST',Ti.API.apiAdress+addOrRemove);
								xhr.send(data);
								   
							
						});
						row6.add(lb_isBookmarked);
						row6.add(sw_isBookmarked);
						
						if(Ti.Network.online==1){
							//Show the View für the UserRating, Functions are after Line 320
							var row7 = Ti.UI.createTableViewRow({
							    height:'auto',
							    selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
							});
							var infoUserRating = Ti.UI.createLabel({
								text:L('yourRating'),
								top:5,
								//width:Ti.UI.FILL,
								left:0,
								color: 'black',
								font: { fontFamily:'Arial', fontSize: '17dp'}
							});	
							var devWidth = (Ti.Platform.displayCaps.platformWidth/2);
							var uRateView = Ti.UI.createView({
								backgroundColor:'transparent',
								layout:'horizontal',
								height:'40dp',
								left:devWidth
							});
							row7.add(infoUserRating);
							row7.add(uRateView);
							
							//Show the Stars for the global Rating of the Casts
							var row8 = Ti.UI.createTableViewRow({
							    height:'auto',
							    selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
							});
							var infoCastRating = Ti.UI.createLabel({
								text:L('rating'),
								top:5,
								width:Ti.UI.FILL,
								color: 'black',
								font: { fontFamily:'Arial', fontSize: '17dp'}
							});
							
							
							var cRateView = Ti.UI.createView({
								backgroundColor:'transparent',
								layout:'horizontal',
								height:'40dp',
								left: devWidth
							});
							row8.add(infoCastRating);
							row8.add(cRateView);
							
							//Textfield für Comment
							var row10=Ti.UI.createTableViewRow({
								height:'auto'
							});
							var tb_Comment=Ti.UI.createTextField({
								hintText:L('Comment'),
								value: cast.properties.uComment,
								borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
								width:Ti.UI.FILL
							});
							row10.add(tb_Comment);
							
							//Button zum Speichern der Änderungen von Ratings und Comments
							var row11 = Ti.UI.createTableViewRow({
								height:'auto'
							});
							var bu_saveRatingAndComment=Ti.UI.createButton({
								title:L('saveComment'),
								color: 'black',
								font: { fontFamily:'Arial', fontSize: '17dp'},
								width:Ti.UI.FILL
							});
						
							//Event to save Ratings and Comments	
							bu_saveRatingAndComment.addEventListener('click',function(e){
								bu_saveRatingAndComment.enabled = false; //avoid doupletapping
									cast.properties.uComment=tb_Comment.value;
									var data={
											dUuid:dUuid,
											cId:cast.properties.id,
											rComment:cast.properties.uComment,
											rRating: cast.properties.uRating
									};
									    var xhr = Ti.Network.createHTTPClient({
											onload: function(e,listData) {
											parsedData = JSON.parse(this.responseText);
											//alert(parsedData);
												if(parsedData.isSuccess){
													alert(L('changesSuccess'));
												}
												else{Titanium.UI.createAlertDialog({ title: L('error'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();}
												bu_saveRatingAndComment.enabled = true;
											}
									    });
									//open json request
									xhr.open('POST',Ti.API.apiAdress+'/rating/insertUpdateRatingToCast');
									xhr.send(data);
									   
							});
							row11.add(bu_saveRatingAndComment);
							
							//Button zum Löschen und Bearbeiten
							var row12 = Ti.UI.createTableViewRow({
								height:'auto'
							});
							var bu_deleteCast=Ti.UI.createButton({
								title:L('deleteCast'),
								color: 'black',
								width: Ti.UI.FILL,
								font: { fontFamily:'Arial', fontSize: '17dp'}
							});
							bu_deleteCast.addEventListener('click',function(e){
								
								var data={
									dUuid:dUuid,
									cId:cast.properties.id,
								};
								
								//lösche Cast
							    var xhr = Ti.Network.createHTTPClient({
									onload: function(e,listData) {
									parsedData = JSON.parse(this.responseText);
										if(parsedData.isSuccess){
											alert(L('changesSuccess'));
											var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,cast.properties.fileName+'.mp3');
											try{
												//Lösche, falls vorhanden, Cast von Filesystem und aus DB
												if(file.exists()){
													file.deleteFile();
													var db = Ti.Database.open('StudiCastDB');
													var deleteCast= db.execute('DELETE FROM StudiCastCasts WHERE id=?;',cast.properties.id);
													db.close();
												}	
											}
											catch(e){
												//Time to go, my People need me!
											}
											self.close();
										}
										else{Titanium.UI.createAlertDialog({ title: L('error'), message: parsedData.message, buttonNames: ['OK'], cancel: 0 }).show();}
									}
							    });
								//open json request
								xhr.open('POST',Ti.API.apiAdress+'/cast/removeCast');
								xhr.send(data);
							});
							var bu_changeCast=Ti.UI.createButton({
								title:'changeCast',
								color: 'black',
								right:10,
								font: { fontFamily:'Arial', fontSize: '17dp'}
							});
							row12.add(bu_deleteCast);
							//row12.add(bu_changesCast);
						}
						
						var rows=[];
						if(Ti.Network.online==1){
							if(cast.properties.authorId==uId){
								rows = [row1,row2,row3,row4,row5, row6, row7, row8, row10, row11, row12];
							}
							else{
								rows = [row1,row2,row3,row4,row5, row6, row7, row8, row10, row11];
							}
							
						}
						else{
							rows = [row1,row2, row4];
						}	
						
						//Table to show the TableRows
						var table = Ti.UI.createTableView({
						    data:rows,
						    backgroundColor: 'transparent',
						    borderWidth: 0,
						    borderColor: 'transparent',
						    separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
						    style: Ti.UI.iPhone.TableViewStyle.PLAIN,
							separatorColor: 'transparent'
						});
						
						self.add(table);
					
					
					
						//Start Rating Functions
						//Variablen, Funktionen und Buttons für Rating
						var max = 5;// max Anzahl an Sterne
						
						function uRate(v) {
						    // save the rate
						     cast.properties.uRating=v;
						    for(var i=1;i<=max;i++){
						    	//show the new rate in Stars
						    	//uRatingStars[i].image = (v >= i) ? goldStar : greyStar;
						    	if(v>=i){
						    		uRatingStars[i].setBackgroundImage(goldStar);
						    	}
						    	else{
						    		uRatingStars[i].setBackgroundImage(greyStar);
						    	}
						    }
						}
					if(Ti.Network.online==1){
						var uRatingStars = [];
						var stars=[];
						var goldStar='/images/Star.png';
						var greyStar='/images/GreyStar.png';
						
						//create Buttons and ImageViews für Ratings
						for(var i=1;i<=max;i++) {
							    uRatingStars[i] = Ti.UI.createButton({
						        // set BackgroundImage for Buttons for userRating
						        backgroundImage: (i<=cast.properties.uRating) ? goldStar : greyStar,
						        width:'30dp',
						        height:'30dp',
						        rating: i
						    });
						    stars[i] = Ti.UI.createImageView({
						        // set ImageViews for castRating
						        image: (i<=cast.properties.cRating) ? '/images/Star.png' : '/images/GreyStar.png',
						        width:'30dp',
						        height:'30dp',
						    });
						    uRatingStars[i].addEventListener('click', function(e) {
						        uRate(e.source.rating);
						    });  
						    uRateView.add(uRatingStars[i]);
						     cRateView.add(stars[i]); 
						}
						//End of Rating Functions
					}
						//save dUuid in Variable	
						var db = Ti.Database.open('StudiCastDB');
						var dUuid = db.execute('SELECT content FROM StudiCastData WHERE name= ?','dUuid').fieldByName('content');
						db.close();	
					
						
	return self;
};

module.exports = castPlayerWindow;