function beforeRecordWindow(Window,win1){
var self = Ti.UI.createWindow({
		title:L('allCasts'),
		backgroundColor:'white',
		layout:'vertical'
	});
// Array der anzuzeigenden Elemente, ggf direkt Casts verwendbar

		getCategories=require('lib/getCategories');
		//var listData = [];
		getCategories(Window, showCategories, self, win1);
		return self;
	}
	
	function showCategories(Window, listData, self, win1){
				// Add the list data items to a section
				var listSection = Titanium.UI.createListSection({items: listData});
				 
				// Add the list section to a list view
				var listView = Titanium.UI.createListView({sections: [listSection]});
					
						
				listView.addEventListener('itemclick', function(e){
				    var item = e.section.getItemAt(e.itemIndex);
				     
				    //hier Aktion durchführen, die mit item durchgeführt werden soll
				    e.section.updateItemAt(e.itemIndex, item);  
				    	var goToRecord = require('ui/common/recordWindow');
						win1.containingTab.open(new goToRecord(Window, win1, item));   
				});		
						
				self.add(listView);	

};

module.exports = beforeRecordWindow;